# Terminal Registry

Launch a terminal command or switch to the buffer if the command is already running.  
See <doc/terminal-registry.txt> for details.
